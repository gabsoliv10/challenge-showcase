// Load JSON
window.onload = function() {  
    var script = document.createElement('script');
    script.src = 'http://roberval.chaordicsystems.com/challenge/challenge.json?callback=X';
    var items = document.getElementsByTagName('script')[0];
    items.parentNode.insertBefore(script, items);
};

// Insert featured into html
function loadFeatured(featured) {
    var featuredItem = '<div class="products__featured-case">'
    + '<a class="products__link products__link--image" href="http:' + featured.item.detailUrl + '">'
    + '<img src="http:' + featured.item.imageName + '" alt="featured" />'
    + '</a>'
    + '<a class="products__link products__link--title" href="http:' + featured.item.detailUrl + '">'
    + '<h1 class="products__title">' + (featured.item.name) + '</h1>'
    + '</a>'
    + '<p class="products__old-price">De: ' + featured.item.oldPrice + '</p>'
    + '<p class="products__price">Por: <span>' + featured.item.price + '</span></p>'
    + '<p class="products__payment">' + featured.item.productInfo.paymentConditions + '</p>'
    + '</a>'
    + '</div>'

    document.getElementById("products__featured-item").innerHTML = featuredItem;
}

// Insert proposal into html
function loadProposal(proposal) {
    var proposalItem = "";
    for (var i = 0; i < proposal.length; i++) {
        proposalItem +=  '<div class="products__proposal-case">'
        + '<a class="products__link products__link--image" href="http:' + proposal[i].detailUrl + '">'
        + '<img src="http:' + proposal[i].imageName + '" alt="proposal" />'
        + '</a>'
        + '<a class="products__link products__link--title" href="http:' + proposal[i].detailUrl + '">'
        + '<h1 class="products__title">' + (proposal[i].name) + '</h1>'
        + '</a>'
        + '<p class="products__old-price">De: ' + proposal[i].oldPrice + '</p>'
        + '<p class="products__price">Por: <span>' + proposal[i].price + '</span></p>'
        + '<p class="products__payment">' + proposal[i].productInfo.paymentConditions + ' sem juros</p>'
        + '</a>'
        + '</div>'
    }
        document.getElementById("products__proposal-items").innerHTML = proposalItem;
}

//Load data and Render functions
function X(data) {
    var proposal = data.data.recommendation;
    loadProposal(proposal);
    var featured = data.data.reference;
    loadFeatured(featured);
}